﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CoreWebSockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace AspNetCore.WebSockets
{
    public abstract class WebSocketHandler
    {
        public WebSocketHandler(WebSocketConnectionManager webSocketConnectionManager)
        {
            WebSocketConnectionManager = webSocketConnectionManager;
        }

        protected WebSocketConnectionManager WebSocketConnectionManager { get; set; }

        public event EventHandler OnConnected;
        public event EventHandler OnDisconnected;

        public IReadOnlyDictionary<string, WebSocket> Clients
        {
            get {
                return new ReadOnlyDictionary<string, WebSocket>(
                    WebSocketConnectionManager.GetAll()
                );
            }
        }

        public async Task SocketConnected(WebSocket socket)
        {
            OnConnected?.Invoke(this, new WebSocketEventArgs(socket));
            WebSocketConnectionManager.AddSocket(socket);
        }

        public async Task SocketDisconnected(WebSocket socket)
        {
            OnDisconnected?.Invoke(this, new WebSocketEventArgs(socket));
            await WebSocketConnectionManager.RemoveSocket(WebSocketConnectionManager.GetId(socket));
        }

        public async Task SendMessageAsync(WebSocket socket, WebSocketMessage message)
        {
            if (socket.State != WebSocketState.Open)
                return;

            var json = JsonConvert.SerializeObject(message, Settings.JsonSerializerSettings);

            await socket.SendAsync(new ArraySegment<byte>(Encoding.ASCII.GetBytes(json), 0, json.Length), WebSocketMessageType.Text, true, CancellationToken.None);
        }

        public async Task SendMessageAsync(string socketId, WebSocketMessage message)
        {
            await SendMessageAsync(WebSocketConnectionManager.GetSocketById(socketId), message);
        }

        public async Task SendMessageToAllAsync(WebSocketMessage message)
        {
            foreach (var pair in WebSocketConnectionManager.GetAll())
                if (pair.Value.State == WebSocketState.Open)
                    await SendMessageAsync(pair.Value, message);
        }

        public Task OnData(WebSocket socket, WebSocketReceiveResult result, byte[] buffer)
        {
            var data = Encoding.UTF8.GetString(buffer, 0, result.Count);
            var message = JsonConvert.DeserializeObject<WebSocketMessage>(data);

            return ReceiveAsync(socket, message);
        }

        public abstract Task ReceiveAsync(WebSocket socket, WebSocketMessage message);
    }
}