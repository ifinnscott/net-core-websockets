﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace CoreWebSockets
{
    public class Settings
    { 
        public static JsonSerializerSettings JsonSerializerSettings { get; set; }
    }
}
