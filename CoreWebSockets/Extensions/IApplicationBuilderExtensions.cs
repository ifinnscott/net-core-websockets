﻿using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace AspNetCore.WebSockets
{
    public static class IApplicationBuilderExtensions
    {
        public static IApplicationBuilder MapWebSocketManager(this IApplicationBuilder app, PathString path, WebSocketHandler handler)
        {
            app.UseWebSockets();
            return app.Map(path, application => application.UseMiddleware<WebSocketManagerMiddleware>(handler));
        }
    }
}