﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AspNetCore.WebSockets
{
    public class WebSocketMessage
    {
        public string Type { get; set; }
        public object Contents { get; set; }
    }
}
