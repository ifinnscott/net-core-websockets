﻿using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;

namespace CoreWebSockets
{
    public class WebSocketEventArgs : EventArgs
    {
        public WebSocketEventArgs(WebSocket socket)
        {
            this.WebSocket = WebSocket;
        }

        public WebSocket WebSocket { get; set; }
    }
}
